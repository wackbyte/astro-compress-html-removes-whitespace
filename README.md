# `astro-compress-html-removes-whitespace`

Demonstrates Astro's `compressHTML` option removing whitespace that should remain.

[Here is the relevant issue.](https://github.com/withastro/astro/issues/7556)

## Testing

Toggle `compressHTML` to see the whitespace be removed when it is enabled.

### Running a development server

```sh
$ pnpm dev
```

### Running a production server

```sh
$ pnpm build
$ pnpm preview
```
